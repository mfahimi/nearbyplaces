package com.mfahimi.cafebazaartask.Data

@Suppress("DataClassPrivateConstructor")
data class NetworkState private constructor(
    val status: Status,
    val msg: String? = null,
    val apiError: ApiError? = null
) {
    companion object {
        val LOADED = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.LOADING)
        fun error(msg: String?, apiError: ApiError) = NetworkState(Status.ERROR, msg, apiError)
    }
}