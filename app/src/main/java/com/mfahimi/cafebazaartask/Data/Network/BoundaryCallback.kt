package com.mfahimi.cafebazaartask.Data.Network

import android.location.Location
import androidx.annotation.MainThread
import androidx.paging.PagedList
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.Data.Network.Model.explore
import com.mfahimi.cafebazaartask.Data.getApiError
import com.mfahimi.cafebazaartask.Util.PagingRequestHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

/**
 * This boundary callback gets notified when user reaches to the edges of the list such that the
 * database cannot provide any more data.
 * <p>
 * The boundary callback might be called multiple times for the same direction so it does its own
 * rate limiting using the PagingRequestHelper class.
 */
class BoundaryCallback(
    private val location: Location,
    private val webservice: ApiService,
    private val handleResponse: (Location, explore.Response.Group?) -> Unit,
    private val ioExecutor: Executor,
    private val networkPageSize: Int,
    private val clientId: String,
    private val secredId: String
) : PagedList.BoundaryCallback<Venue>() {

    var lastRequestedPage = 0
    val helper = PagingRequestHelper(ioExecutor)
    val networkState = helper.createStatusLiveData()

    /**
     * Database returned 0 items. We should query the backend for more items.
     */
    @MainThread
    override fun onZeroItemsLoaded() {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            webservice.explore(
                clientId,
                secredId,
                networkPageSize,
                networkPageSize * lastRequestedPage,
                "${location.latitude},${location.longitude}"
            ).enqueue(createWebserviceCallback(it))
        }
    }

    /**
     * User reached to the end of the list.
     */
    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: Venue) {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            webservice.explore(
                clientId,
                secredId,
                networkPageSize,
                networkPageSize * lastRequestedPage,
                "${location.latitude},${location.longitude}"
            ).enqueue(createWebserviceCallback(it))
        }
    }

    /**
     * every time it gets new items, boundary callback simply inserts them into the database and
     * paging library takes care of refreshing the list if necessary.
     */
    private fun insertItemsIntoDb(response: Response<explore>, it: PagingRequestHelper.Request.Callback) {
        ioExecutor.execute {
            handleResponse(location, response.body()?.response?.groups!![0])
            it.recordSuccess()
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: Venue) {
        // ignored, since we only ever append to what's in the DB
    }

    private fun createWebserviceCallback(it: PagingRequestHelper.Request.Callback)
            : Callback<explore> {
        return object : Callback<explore> {
            override fun onFailure(call: Call<explore>, t: Throwable) {
                it.recordFailure(t, t.getApiError())
            }

            override fun onResponse(call: Call<explore>, response: Response<explore>) {
                if (response.isSuccessful && response.code() == 200) {
                    lastRequestedPage++
                    insertItemsIntoDb(response, it)
                } else
                    it.recordFailure(null, response.getApiError())

            }
        }
    }
}