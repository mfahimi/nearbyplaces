package com.mfahimi.cafebazaartask.Data.Network

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.mfahimi.cafebazaartask.Data.ApiError
import com.mfahimi.cafebazaartask.Data.Resource

/**
 * A generic class that can provide a resource backed by network.
 *
 * @param <RequestType>
</RequestType>*/
abstract class NetworkResource<RequestType> {

    private val result = MediatorLiveData<Resource<RequestType>>()

    init {
        result.value = Resource.loading(null)
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()

        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> setValue(Resource.success(response.body))
                is ApiEmptyResponse -> setValue(
                    Resource.error(
                        null,
                        null,
                        ApiError.ERROR_NO_CONTENT
                    )
                )
                is ApiErrorResponse -> {
                    onFetchFailed()
                    setValue(
                        Resource.error(
                            response.errorMessage,
                            null,
                            response.error
                        )
                    )
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<RequestType>>

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}
