package com.mfahimi.cafebazaartask.Data.Local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.mfahimi.cafebazaartask.Data.Network.Model.VenueDetail

@Dao
abstract class DetialDao : BaseDao<VenueDetail> {

    @Query("SELECT * FROM venuedetail WHERE venueid=:venueId")
    abstract fun getVenuDetail(venueId: String): LiveData<VenueDetail>
}