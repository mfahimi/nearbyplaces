package com.mfahimi.cafebazaartask.Data.Network.Model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class explore(
    @SerializedName("meta")
    val meta: Meta?,
    @SerializedName("response")
    val response: Response?
) {
    data class Response(
        @SerializedName("groups")
        val groups: List<Group>,
        @SerializedName("totalResults")
        val totalResults: Int?
    ) {
        data class Group(
            @SerializedName("items")
            val items: List<Item>
        ) {
            data class Item(
                @SerializedName("venue")
                val venue: Venue
            )
        }
    }

    data class Location(
        @SerializedName("address")
        val address: String?,
        @SerializedName("cc")
        val cc: String?,
        @SerializedName("city")
        val city: String?,
        @SerializedName("country")
        val country: String?,
        @SerializedName("distance")
        val distance: Int?,
        @SerializedName("lat")
        val lat: Double?,
        @SerializedName("lng")
        val lng: Double?,
        @SerializedName("postalCode")
        val postalCode: String?,
        @SerializedName("state")
        val state: String?
    )

    data class Meta(
        @SerializedName("code")
        val code: Int?,
        @SerializedName("requestId")
        val requestId: String?
    )
}

@Entity(tableName = "Venue")
data class Venue(
    @PrimaryKey
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @Embedded
    @SerializedName("location")
    var location: explore.Location? = null
) {
    // does not show up in the response but set in save processing.
    var search_lat: Double = 0.toDouble()
    // does not show up in the response but set in save processing.
    var search_lng: Double = 0.toDouble()
}