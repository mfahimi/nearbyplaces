package com.mfahimi.cafebazaartask.Data.Network.Model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import com.google.gson.annotations.SerializedName

@Entity(
    primaryKeys = arrayOf("venueid"),
    tableName = "VenueDetail"
    , foreignKeys = [ForeignKey(
        entity = Venue::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("venueid")
        , onDelete = CASCADE, onUpdate = CASCADE
    )]
)
data class VenueDetail(
    @Embedded
    @SerializedName("response")
    val response: Response
) {
    data class Response(
        @Embedded(prefix = "venue")
        @SerializedName("venue")
        val venue: Venue
    ) {
        data class Venue(
            @SerializedName("allowMenuUrlEdit")
            val allowMenuUrlEdit: Boolean?,
            @Embedded(prefix = "bestPhoto")
            @SerializedName("bestPhoto")
            val bestPhoto: BestPhoto?,
            @SerializedName("canonicalUrl")
            val canonicalUrl: String?,
            @SerializedName("createdAt")
            val createdAt: Long?,
            @SerializedName("dislike")
            val dislike: Boolean?,
            @SerializedName("hasMenu")
            val hasMenu: Boolean?,
            @SerializedName("id")
            val id: String,
            @Embedded(prefix = "location")
            @SerializedName("location")
            val location: Location?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("ok")
            val ok: Boolean?,
            @SerializedName("rating")
            val rating: Double?,
            @SerializedName("ratingColor")
            val ratingColor: String?,
            @SerializedName("ratingSignals")
            val ratingSignals: Int?,
            @SerializedName("shortUrl")
            val shortUrl: String?,
            @SerializedName("timeZone")
            val timeZone: String?,
            @SerializedName("verified")
            val verified: Boolean?
        ) {

            data class Location(
                @SerializedName("address")
                val address: String?,
                @SerializedName("cc")
                val cc: String?,
                @SerializedName("city")
                val city: String?,
                @SerializedName("country")
                val country: String?,
                @SerializedName("crossStreet")
                val crossStreet: String?,
                @SerializedName("lat")
                val lat: Double?,
                @SerializedName("lng")
                val lng: Double?,
                @SerializedName("postalCode")
                val postalCode: String?,
                @SerializedName("state")
                val state: String?
            )

            data class BestPhoto(
                @SerializedName("createdAt")
                val createdAt: Int?,
                @SerializedName("height")
                val height: Int?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("prefix")
                val prefix: String?,
                @SerializedName("suffix")
                val suffix: String?,
                @SerializedName("visibility")
                val visibility: String?,
                @SerializedName("width")
                val width: Int?
            )

        }
    }
}