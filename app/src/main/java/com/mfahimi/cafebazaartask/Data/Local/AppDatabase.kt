package com.mfahimi.cafebazaartask.Data.Local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.Data.Network.Model.VenueDetail

@Database(entities = [Venue::class, VenueDetail::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getExploreDao(): ExploreDao
    abstract fun getVenueDetailDao(): DetialDao

}