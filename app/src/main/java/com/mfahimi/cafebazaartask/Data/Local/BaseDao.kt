package com.mfahimi.cafebazaartask.Data.Local

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg entity: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entity: List<T>)

    @Update
    fun update(entity: T)

    @Update
    fun updateAll(vararg entity: T)

    @Delete
    fun delete(entity: T)
}