package com.mfahimi.cafebazaartask.Di

import com.mfahimi.cafebazaartask.Ui.Main.MainActivity
import com.mfahimi.cafebazaartask.Ui.Main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}
