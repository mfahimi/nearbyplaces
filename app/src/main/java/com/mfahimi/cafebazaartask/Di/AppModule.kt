package com.mfahimi.cafebazaartask.Di

import android.app.Application
import androidx.room.Room
import com.mfahimi.cafebazaartask.BuildConfig
import com.mfahimi.cafebazaartask.Data.Local.AppDatabase
import com.mfahimi.cafebazaartask.Data.Network.ApiService
import com.mfahimi.cafebazaartask.Util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {
    @Singleton
    @Provides
    fun provideGithubService(executors: AppExecutors): ApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        if (BuildConfig.DEBUG)
            httpClient.addInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            )


        return Retrofit.Builder()
            .baseUrl("https://api.foursquare.com/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .callbackExecutor(executors.networkIO())
            .client(httpClient.build())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, "applicationdb.db")
            .fallbackToDestructiveMigration()
            .build()
    }
}
