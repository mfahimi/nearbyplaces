package com.mfahimi.cafebazaartask.Di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
