package com.mfahimi.cafebazaartask.Util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

object PermissionUtils {
    val LOCATION_PERMISSION_ID = 17

    var LOCATION_PERMISSION = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    /*check if app has requested permission*/
    fun hashPermission(context: Context?, permissions: Array<String>): Boolean {
        if (context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    /**
     * Requests the fine location permission. If a rationale with an additional explanation should
     * be shown to the user, displays a dialog that triggers the request.
     */
    fun requestPermission(
        activity: AppCompatActivity, requestCode: Int,
        permission: Array<String>
    ) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission[0])) {
            // Display a dialog with rationale.
            if (LOCATION_PERMISSION_ID == requestCode)
            //                showLocationPermissionRationale(activity, requestCode);
                ActivityCompat.requestPermissions(activity, LOCATION_PERMISSION, requestCode)
        } else {
            // Location permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, permission, requestCode)

        }
    }

    fun isPermissionGranted(
        grantPermissions: Array<String>, grantResults: IntArray,
        permission: String
    ): Boolean {
        for (i in grantPermissions.indices) {
            if (permission == grantPermissions[i]) {
                return grantResults[i] == PackageManager.PERMISSION_GRANTED
            }
        }
        return false
    }

}