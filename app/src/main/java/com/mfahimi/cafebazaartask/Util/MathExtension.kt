package com.mfahimi.cafebazaartask.Util

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


fun Double.roundLocation(): Double {
    val formatter = NumberFormat.getNumberInstance(Locale.US) as DecimalFormat
    formatter.applyPattern("##.####")
    formatter.roundingMode = RoundingMode.HALF_EVEN
    return formatter.format(this).toDouble()
}