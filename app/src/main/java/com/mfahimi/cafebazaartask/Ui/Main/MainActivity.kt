package com.mfahimi.cafebazaartask.Ui.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.R
import com.mfahimi.cafebazaartask.Ui.Main.Detail.DetailArgs
import com.mfahimi.cafebazaartask.Ui.Main.Detail.DetailFragment
import com.mfahimi.cafebazaartask.Ui.Main.List.ListFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
    HasSupportFragmentInjector, ListFragment.ListFragmentListener {


    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    @Inject
    lateinit var listFragment: ListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, listFragment)
            .commit()
    }

    override fun onListItemClick(venue: Venue) {
        val args = DetailArgs(venue.id, venue.name)
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
            .replace(R.id.fragmentContainer, DetailFragment.newInstance(args))
            .addToBackStack(null)
            .commit()
    }
}
