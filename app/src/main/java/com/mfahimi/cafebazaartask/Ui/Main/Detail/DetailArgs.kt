package com.mfahimi.cafebazaartask.Ui.Main.Detail

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DetailArgs(val venueId: String, val title: String) : Parcelable