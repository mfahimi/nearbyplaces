package com.mfahimi.cafebazaartask.Ui.Main

import com.mfahimi.cafebazaartask.Ui.Main.Detail.DetailFragment
import com.mfahimi.cafebazaartask.Ui.Main.List.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector
    abstract fun listFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun detailFragment(): DetailFragment

}