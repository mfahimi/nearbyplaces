package com.mfahimi.cafebazaartask.Ui.Main.List

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.Data.NetworkState
import com.mfahimi.cafebazaartask.Data.Status
import com.mfahimi.cafebazaartask.R


class ListAdapter(
    private val retryCallback: () -> Unit,
    private val detailCallback: (Venue) -> Unit
) : PagedListAdapter<Venue, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_location_list -> (holder as DataHolder).bind(getItem(position))
            R.layout.item_loading -> (holder as LoadingHolder).bindTo(networkState)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_location_list -> DataHolder.create(
                parent,
                detailCallback
            )
            R.layout.item_loading -> LoadingHolder.create(
                parent,
                retryCallback
            )
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_loading
        } else {
            R.layout.item_location_list
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {

        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Venue>() {
            override fun areItemsTheSame(oldItem: Venue, newItem: Venue) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Venue, newItem: Venue) =
                oldItem.equals(newItem)

        }
    }
}

class DataHolder(
    view: View,
    private val detailCallback: (Venue) -> Unit
) : RecyclerView.ViewHolder(view) {
    val title: TextView = view.findViewById(R.id.title)
    val address: TextView = view.findViewById(R.id.address)

    fun bind(venue: Venue?) {
        title.setText(venue?.name)
        address.setText(venue?.location?.address)
        itemView.setOnClickListener {
            venue?.id?.let {
                detailCallback(venue)
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup, detailCallback: (Venue) -> Unit): DataHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_location_list, parent, false)
            return DataHolder(view, detailCallback = detailCallback)
        }
    }
}

class LoadingHolder(
    view: View,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(view) {
    private val progressBar = view.findViewById<ProgressBar>(R.id.progress_bar)
    private val retry = view.findViewById<Button>(R.id.retry_button)

    init {
        retry.setOnClickListener {
            retryCallback()
        }
    }

    fun bindTo(networkState: NetworkState?) {
        progressBar.visibility =
            toVisibility(networkState?.status == Status.LOADING)
        retry.visibility =
            toVisibility(networkState?.status == Status.ERROR)
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): LoadingHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_loading, parent, false)
            return LoadingHolder(view, retryCallback)
        }

        fun toVisibility(constraint: Boolean): Int {
            return if (constraint) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
}
