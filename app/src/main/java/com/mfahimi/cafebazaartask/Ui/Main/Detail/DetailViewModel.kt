package com.mfahimi.cafebazaartask.Ui.Main.Detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mfahimi.cafebazaartask.Repository.DetailRepository
import javax.inject.Inject

class DetailViewModel @Inject constructor(val repository: DetailRepository) : ViewModel() {
    private val _venueId: MutableLiveData<String> = MutableLiveData()
    val venueId: LiveData<String>
        get() = _venueId

    val detail = Transformations.switchMap(venueId) {
        repository.getVenueDetail(it)
    }

    fun init(venId: String) {
        if (venId.isNotEmpty() && venId != _venueId.value)
            _venueId.value = venId
    }

    fun retry(venId: String) {
        if (venId.isNotEmpty()) {
            _venueId.value = venId
        }
    }


}