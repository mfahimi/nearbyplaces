package com.mfahimi.cafebazaartask.Ui.Main.List

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mfahimi.cafebazaartask.Repository.ListRepository
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel @Inject constructor(private val repository: ListRepository) : ViewModel() {
    private val location = MutableLiveData<Location>()

     val repoResult = Transformations.map(location) { repository.explore(it) }
    val posts = Transformations.switchMap(repoResult) { it.pagedList }!!
    val networkState = Transformations.switchMap(repoResult) { it.networkState }!!
    val refreshState = Transformations.switchMap(repoResult) { it.refreshState }!!


    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun showVenues(ll: Location): Boolean {
        if (location.value == ll) {
            return false
        }
        location.value = ll
        return true
    }

    fun loadOldDataIfAvaliable() {
        repository.getLastLocation()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<com.mfahimi.cafebazaartask.Data.Local.Location> {
                override fun onSuccess(t: com.mfahimi.cafebazaartask.Data.Local.Location) {
                    val location = Location("gps")
                    location.latitude = t.lat
                    location.longitude = t.lng
                    showVenues(location)
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                }


            })
    }

    fun retry() {
        repoResult?.value?.retry?.invoke()
    }

    fun currentLocation(): Location? = location.value
}