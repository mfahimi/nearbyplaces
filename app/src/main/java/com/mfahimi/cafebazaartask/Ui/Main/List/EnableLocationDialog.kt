package com.mfahimi.cafebazaartask.UI.List

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_fragment_enable_location.*


class EnableLocationDialog : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.mfahimi.cafebazaartask.R.layout.dialog_fragment_enable_location, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // request a window without the title
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        yes.setOnClickListener {
            setResultBack(true)
        }
        no.setOnClickListener {

            setResultBack(false)
        }
    }

    private fun setResultBack(boolean: Boolean) {
        val listener = targetFragment as onClickListener
        listener.onClick(boolean)
        dismiss()
    }

    interface onClickListener {
        fun onClick(turnOnLocation: Boolean)
    }
}